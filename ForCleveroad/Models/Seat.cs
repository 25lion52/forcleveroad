﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForCleveroad.Models
{
    public class Seat
    {
        public int SeatId { get; set; }
        public int? OrderId { get; set; }
        public string Type { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
        public decimal Price { get; set; }
        public virtual Order Order { get; set; }
    }
}