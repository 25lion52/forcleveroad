﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForCleveroad.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public float Price { get; set; }
        public virtual IList<Seat> Seats { get; set; }
        //public DateTime 
    }
}