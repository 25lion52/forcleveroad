﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForCleveroad.Models
{
    public class OrderForAjax
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public float Price { get; set; }
        public int CountSeats { get; set; }
        public int id { get; set; }
    }
}