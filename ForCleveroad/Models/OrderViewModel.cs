﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ForCleveroad.Models
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {

        }

        public OrderViewModel(IList<Seat> allSeats)
        {
            this.AllSeats = allSeats;
        }

        public OrderViewModel(Order ord)
        {
            OrderToViewModel(ord);
        }

        public OrderViewModel(Order ord, IList<Seat> allSeats)
        {
            OrderToViewModel(ord);
            this.AllSeats = allSeats;
        }

        public int OrderId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public float Price { get; set; }
        public virtual IList<Seat> Seats { get; set; }

        public IList<Seat> AllSeats { get; set; }

        public static Order Map(OrderViewModel viewModel)
        {
            Order newOrder = new Order();
            newOrder.OrderId = viewModel.OrderId;
            newOrder.Email = viewModel.Email;
            newOrder.Name = viewModel.Name;
            newOrder.Price = viewModel.Price;
            newOrder.Seats = viewModel.Seats;

            return newOrder;
        }

        private void OrderToViewModel(Order ord)
        {
            this.OrderId = ord.OrderId;
            this.Name = ord.Name;
            this.Email = ord.Email;
            this.Price = ord.Price;
            this.Seats = ord.Seats;
        }
    }
}