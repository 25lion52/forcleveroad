﻿$(document).ready(function () {
    LoadNewTable();
});

function LoadNewTable() {
    $.ajax({
        url: 'Home/GetTable',
        type: 'POST',
        dataType: 'json',
        success: function (result) {
                // ASP.NET encapsulates JSON responses in a property "d", so remove it
            if (result.hasOwnProperty("d"))
            {
                result = result.d;
            }
                var data = result;


                $('#divformytable').html('<table cellpadding="0" cellspacing="0" border="0" id="tablename" class="style1 datatable"></table>');
                var table = $('#myTable').DataTable({
                    "bPaginate": true,
                    "sPaginationType": "full_numbers",
                    "bAutoWidth": true,
                    "bJQueryUI": false,
                    "bLengthChange": true,
                    //"fnRowCallback": customFnRowCallback,
                    "aaData": data,
                    "aoColumns": [
                   //Assign the data to rows
                    { "mDataProp": "Name", "sTitle": "Name", "bSearchable": true, "bVisible": true },
                    { "mDataProp": "Email", "sTitle": "Email", "bSearchable": false, "bVisible": true },
                    { "mDataProp": "Price", "sTitle": "Price", "bSearchable": false, "bVisible": true },
                    { "mDataProp": "CountSeats", "sTitle": "Seats", "bSearchable": false, "bVisible": true },
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<button class=\"editBtn\">Edit</button>"
                            + "<button class=\"delBtn\">Delete</button>",
                    },
                    { "mDataProp": "id", "sType": "html", "bSearchable": false, "bVisible": false }
                    ]
                });

                $('#myTable tbody').on('click', '.editBtn', function () {
                    var dataRow = table.row($(this).parents('tr')).data();
                    //alert(data[0] + "'s salary is: " + title);
                    window.location.href = "/Home/Edit/" + dataRow.id;
                });

                $('#myTable tbody').on('click', '.delBtn', function (e) {
                    var thisRow = table.row($(this).parents('tr'));
                    var dataRow = thisRow.data();
                    var orderId = dataRow.id;
                    $.ajax({
                            url: 'Home/Delete',
                            type: 'POST',
                            data: { id: orderId },
                            dataType: 'json',
                            success: function (resultJson) {
                                alert(resultJson.Message);
                                if (resultJson.HasDeleted) {
                                    thisRow
                                    .remove()
                                    .draw();
                                } else {
                                    window.location.pathname = "/Home/";
                                }
                            }
                        });
                });


            //$('#myTable').DataTable({
            //    "ajaz": resultJson,
            //    "columns": [
            //            { "data": "Name" },
            //            { "data": "Email" },
            //            { "data": "Price" }
            //    ]
            //});
        }
    });
}