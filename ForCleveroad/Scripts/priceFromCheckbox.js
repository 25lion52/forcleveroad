﻿$(document).ready(function ()
{
    $('.checkboxes').change(function ()
    {
        var price = 0;
        t = this.id;
        switch (t[0])
        {
            case "A":
                price = 25;
                break;
            case "B":
                price = 15;
                break;
            default:
                price = 10;
        }

        if ($(this).is(":checked"))
        {
            var a = parseFloat($('#Price').val());
            $('#Price').val(a + price);
        }
        else
        {
            var a = parseFloat($('#Price').val());
            $('#Price').val(a - price);
        }
        
    });
});