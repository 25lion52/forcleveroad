﻿using ForCleveroad.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ForCleveroad.DAL
{
    public class StartOrderData : DropCreateDatabaseIfModelChanges<OrdersContext> 
    {
        private const int ROW = 10;
        private const int COLUMN = 25;
        private const int TYPE_A = 2;
        private const int TYPE_B = 2;

        protected override void Seed(OrdersContext context)
        {
            var seats = new List<Seat>();
            for (int i = 1; i <= ROW; i++)
            {
                for(int j = 1; j <= COLUMN; j++)
                {
                    if (j <= TYPE_B || j > COLUMN - TYPE_B)
                    {
                        seats.Add(new Seat { Type = i <= TYPE_A ? "A" : "B", Row = i, Column = j, Price = i <= TYPE_A ? 25 : 15 });
                    }
                    else
                    {
                        seats.Add(new Seat { Type = i <= TYPE_A ? "A" : "C", Row = i, Column = j, Price = i <= TYPE_A ? 25 : 10 });
                    }
                }
            }

            seats.ForEach(s => context.Seats.Add(s));
            context.SaveChanges();
        }
    }
}