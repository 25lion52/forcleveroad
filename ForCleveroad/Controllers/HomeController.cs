﻿using ForCleveroad.DAL;
using ForCleveroad.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ForCleveroad.Controllers
{
    public class HomeController : Controller
    {
        private OrdersContext db = new OrdersContext();
        //
        // GET: /Home/

        public ActionResult Index()
        {

            return View(db.Orders.ToList());
        }

        //
        // GET: /Home/Create

        public ActionResult Create()
        {
            OrderViewModel orders = new OrderViewModel(db.Seats.ToList<Seat>());
            ViewBag.CreateOrEdit = "Create";
            return View("CreateOrEdit", orders);
        }

        //
        // POST: /Home/Create

        [HttpPost]
        public ActionResult Create(OrderViewModel orderVM, int[] selectedSeats)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    orderVM.Seats = new List<Seat>();
                    bool raceCondition = false;
                    if (selectedSeats != null)
                    {
                        var selectedSeatsHS = new HashSet<int>(selectedSeats);
                        foreach (var seat in db.Seats)
                        {
                            if (selectedSeatsHS.Contains(seat.SeatId))
                            {
                                if (seat.OrderId == null)
                                {
                                    orderVM.Seats.Add(seat);
                                }
                                else
                                {
                                    raceCondition = true;
                                    this.ModelState.AddModelError(string.Empty, "Free seats have changed");
                                    break;
                                }
                            }
                        }

                        if (!raceCondition)
                        {
                            var order = OrderViewModel.Map(orderVM);
                            order.Price = GetPrice(order.Seats.ToList());
                            db.Orders.Add(order);
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        this.ModelState.AddModelError(string.Empty, "You didn`t check any seats");
                    }
                }

                ViewBag.CreateOrEdit = "Create";
                orderVM.AllSeats = db.Seats.ToList<Seat>();
                return View("CreateOrEdit", orderVM);
            }
            catch
            {
                ViewBag.CreateOrEdit = "Create";
                orderVM.AllSeats = db.Seats.ToList<Seat>();
                return View("CreateOrEdit", orderVM);
            }
        }

        //
        // GET: /Home/Edit/5

        public ActionResult Edit(int id)
        {
            ViewBag.CreateOrEdit = "Edit";
            OrderViewModel orders = new OrderViewModel(db.Orders.First(o => o.OrderId == id), db.Seats.ToList<Seat>());
            return View("CreateOrEdit", orders);
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, OrderViewModel orderVM, int[] selectedSeats)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    orderVM.OrderId = id;
                    orderVM.Seats = new List<Seat>();
                    bool raceCondition = false;
                    if (selectedSeats != null)
                    {
                        var selectedSeatsHS = new HashSet<int>(selectedSeats);
                        foreach (var seat in db.Seats)
                        {
                            if (selectedSeatsHS.Contains(seat.SeatId))
                            {
                                if (seat.OrderId == null || seat.OrderId == id)
                                {
                                    orderVM.Seats.Add(seat);
                                    seat.OrderId = id;
                                    db.Entry(seat).State = EntityState.Modified;
                                }
                                else
                                {
                                    raceCondition = true;
                                    this.ModelState.AddModelError(string.Empty, "Free seats have changed");
                                    break;
                                }
                            }
                            else if (seat.OrderId == id)
                            {
                                seat.OrderId = null;
                                db.Entry(seat).State = EntityState.Modified;
                            }

                        }

                        if (!raceCondition)
                        {
                            var order = OrderViewModel.Map(orderVM);
                            order.Price = GetPrice(order.Seats.ToList());
                            db.Entry(order).State = EntityState.Modified;
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        this.ModelState.AddModelError(string.Empty, "You didn`t check any seats");
                    }
                }

                ViewBag.CreateOrEdit = "Edit";
                OrderViewModel orders = new OrderViewModel(db.Orders.First(o => o.OrderId == id), db.Seats.ToList<Seat>());
                return View("CreateOrEdit", orders);
            }
            catch
            {
                ViewBag.CreateOrEdit = "Edit";
                OrderViewModel orders = new OrderViewModel(db.Orders.First(o => o.OrderId == id), db.Seats.ToList<Seat>());
                return View("CreateOrEdit", orders);
            }
        }

        
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                Order order = db.Orders.Find(id);

                for (int i = order.Seats.Count - 1; i >= 0; i--)
                {
                    var s = order.Seats[i];
                    s.Order = null;
                    db.Entry(s).State = EntityState.Modified;
                }

                db.Orders.Remove(order);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message, HasDeleted = false });
            }

            return Json(new 
            {
                Message = "Order was deleted successfully"
                ,
                HasDeleted = true
            });
        }

        [HttpPost]
        public JsonResult GetTable()
        {
            List<OrderForAjax> list = new List<OrderForAjax>();
            var orders = db.Orders.ToList();
            foreach(Order ord in orders)
            {
                list.Add(new OrderForAjax  { Name = ord.Name, Price = ord.Price, Email = ord.Email, CountSeats = ord.Seats.Count, id = ord.OrderId });
            }
            var result = Json(list);
            return result;
        }

        private float GetPrice(List<Seat> seats)
        {
            float result = 0;
            foreach(Seat seat in seats)
            {
                switch(seat.Type)
                {
                    case "A":
                        result += 25;
                        break;
                    case "B":
                        result += 15;
                        break;
                    default:
                        result += 10;
                        break;
                }
            }

            return result;
        }
    }
}
